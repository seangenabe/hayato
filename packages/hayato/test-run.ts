import { Hayato } from './index'
import { IActionCommand } from '@hayato/core/types'

const app = new Hayato()
async function run() {
  try {
    const adder: IActionCommand = {
      title: 'Adder',
      key: 'add',
      params: {
        augend: {
          type: 'param'
        },
        addend: {
          type: 'param'
        }
      },
      invoke({ args }) {
        const { augend, addend } = args
        return `The sum is ${Number(augend) + Number(addend)}`
      }
    }
    app.addCommand({
      title: 'Operations',
      key: 'ops',
      children: [adder]
    })
    app.addCommand({
      title: 'Debug',
      key: 'debug',
      invoke() {
        debugger
        return 'debugged'
      }
    })
    await app.client.login(require('./creds.json').token)
  } catch (err) {
    console.error('top err', err)
    process.exit(1)
  }
}

process.on('unhandledRejection', (err, p) => {
  console.error('unhandled', err)
})

run()
