import { App } from '@hayato/core'
import { getThemeForApp } from '@hayato/theme'
import { getStoreForApp } from '@hayato/store'
import { autodelete } from '@hayato/plugin-autodelete'
import { echoErrors } from '@hayato/plugin-echo-errors'
import { help } from '@hayato/plugin-help'
import { healthz } from '@hayato/plugin-healthz'

export class Hayato extends App {
  public readonly theme = getThemeForApp(this)
  public readonly store = getStoreForApp(this)

  constructor() {
    super()
    autodelete(this, { store: this.store })
    echoErrors(this, { theme: this.theme })
    help(this, { theme: this.theme })
    healthz(this)
  }
}
