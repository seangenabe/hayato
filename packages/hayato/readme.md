# Hayato

A modular Discord bot framework using Discord.js

## Core features

These core features are available on `@hayato/core` if you want to build upon them without the additional features.

* swappable Client
* radix trie-based command router via `xerpath`
* logging via `pino`
* observables-based message passing via `rxjs`
* queue everything for teardown

## Additional features

These additional features are available separately as additional modules.

* store - data storage
* theme - theming support
* autodelete - automatically delete replies
* echo-errors - echo errors
* help - provide a help command
* healthz - HTTP health check
