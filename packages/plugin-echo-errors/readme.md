# plugin-echo-errors

Echo errors

## Usage

```ts
import echoErrors from '@hayato/echo-errors'

echoErrors(app, { theme })
```

Note: Plugins must throw an instance of `PublicError` to publicly show the error's message.
