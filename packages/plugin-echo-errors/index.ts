import { App } from '@hayato/core'
import { ITheme } from '@hayato/theme'

export function echoErrors(app: App, { theme }: { theme: ITheme }) {
  app.observables.invocationError.subscribe(({ message, error }) =>
    app.call(async () => {
      await message.channel.send({
        embed: {
          color: theme.failureCommand,
          title: 'Error occurred',
          description: error.name === 'PublicError' ? error.message : undefined
        }
      })
    })
  )
}

export default echoErrors
