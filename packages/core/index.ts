import { PublicError } from '@hayato/public-error'
import { ok } from 'assert'
import {
  Client,
  DMChannel,
  GroupDMChannel,
  Message,
  MessageOptions,
  MessageReaction,
  TextChannel,
  User
} from 'discord.js'
import { fullStack } from 'make-error-cause'
import OpenRadixTrie from 'open-radix-trie'
import pDefer from 'p-defer'
import pino, { Logger } from 'pino'
import { BehaviorSubject, fromEvent, Subject, Subscription } from 'rxjs'
import { filter } from 'rxjs/operators'
import { weakMemoizeUnary } from 'weak-memoize-unary'
import { concatExtensiblePath, ExtensiblePath } from 'xerpath'
import {
  assertIsObject,
  buildParams,
  createRouteContext,
  isActionCommand,
  isRerouteObject,
  isSendMessage,
  normalizeSendMessage
} from './helpers'
import {
  AppObservables,
  ClientObservables,
  Command,
  CommandInvocationResult,
  IActionCommand,
  IAppSubjects,
  IClientSubjects,
  ICommandContext,
  ICommandInvocationObservables,
  IRouteContext,
  ISend,
  ISendMessage,
  MaybePromise,
  REROUTE_HELP
} from './types'

const EMOJI_L = '◀'
const EMOJI_R = '▶'

export class App {
  //#region Properties
  private _client: Client
  public get client() {
    return this._client
  }
  public set client(value: Client) {
    this._client = value
    this.onClientChanged(value)
  }
  private readonly clientSubjects: IClientSubjects
  private subscriptionForCurrentClient?: Subscription
  public readonly clientObservables: ClientObservables
  protected _router: OpenRadixTrie<IActionCommand, IRouteContext>
  public get router() {
    return this._router
  }
  public log: Logger = pino()
  private _commands: Command[] = []
  public get commands(): ReadonlyArray<Command> {
    return this._commands
  }
  private _name: string
  public get name() {
    return this._name
  }
  public set name(value: string) {
    this._name = value
    this.subjects.nameChange.next(value)
  }
  private _observables: AppObservables
  public get observables() {
    return this._observables
  }
  private subjects: IAppSubjects
  private asyncTeardownFns: (() => MaybePromise<void>)[] = []
  public getPrefix: (message: Message) => MaybePromise<string> = () =>
    this.name + ' '
  //#endregion

  constructor() {
    this._name = 'hayato'
    this._client = new Client()
    this._router = new OpenRadixTrie(createRouteContext())

    this.subjects = {
      invocationError: new Subject(),
      reply: new Subject(),
      unrouted: new Subject(),
      requestHelpCommand: new Subject(),
      commandAdded: new Subject(),
      error: new Subject(),
      routerNoMatch: new Subject(),
      nameChange: new BehaviorSubject(this._name)
    }
    this._observables = this.createObservables()

    this.clientSubjects = {
      message: new Subject(),
      messageReactionAdd: new Subject(),
      messageDelete: new Subject(),
      ready: new Subject(),
      error: new Subject(),
      warn: new Subject()
    }
    this.clientObservables = getObservablesFromSubjects(this.clientSubjects)

    this.init()
  }

  private init() {
    this.subjects.nameChange.subscribe(name => {
      this.log = pino({
        prettyPrint: process.env.NODE_ENV !== 'production',
        level: 'debug',
        name
      })
    })

    this.subjects.error.subscribe(error => this.log.error(error))

    this.clientObservables.ready.subscribe(() => this.log.info('ready'))
    this.clientObservables.error.subscribe(err => this.log.error(err))
    this.clientObservables.warn.subscribe(info => this.log.warn(info))

    this.clientObservables.message
      .pipe(
        // Do not process own messages
        // Do not process messages from bots
        filter(
          message =>
            message.author.id !== this.client.user.id && !message.author.bot
        )
      )
      .subscribe(message => this.onReceivedMessage(message))
  }

  private onClientChanged(x: Client) {
    // Unattach observables from previous client
    if (this.subscriptionForCurrentClient) {
      this.subscriptionForCurrentClient.unsubscribe()
    }

    // Create client observables
    const observablesForX = createClientObservables(x)

    // Attach observables from new client
    this.subscriptionForCurrentClient = appObserveClient(
      this.clientSubjects,
      observablesForX
    )
  }

  private async onReceivedMessage(message: Message) {
    let prefix: string = await this.getPrefix(message)
    let content: string = message.content

    if (message.channel instanceof DMChannel) {
      // DMing does not require prefix.
      prefix = ''
    } else if (content.startsWith(`<@${this.client.user.id}>`)) {
      // Starts with @ mention.
      prefix = ''
      content = content.substr(prefix.length)
      // Remove spaces after mention.
      content = content.replace(/^\s/, '')
    } else if (content.startsWith(prefix)) {
      // Starts with prefix.
      content = content.substr(prefix.length)
    } else {
      this.subjects.unrouted.next(message)
      return
    }

    await this.processMessageContent({ prefix, message, content })
  }

  private async processMessageContent(opts: {
    message: Message
    content: string
    prefix: string
  }): Promise<void> {
    const { message, content, prefix } = opts
    const { router, log } = this
    const { channel } = message

    log.debug({ content }, `Processing message content`)

    if (this.commands.length === 0) {
      await channel.send(
        `Thank you for using ${
          this.name
        }! There are no commands available right now.`
      )
      return
    }

    const routingResult = router.get(content)
    if (routingResult != null) {
      try {
        const { args } = routingResult
        const routingArgsObj: Record<string, string> = {}
        for (let [key, value] of args as [string, string][]) {
          routingArgsObj[key] = value.trim()
        }

        const command = routingResult.value

        if (!command) {
          if (this.subjects.routerNoMatch.observers.length === 0) {
            await channel.send('Command not matched.')
          } else {
            this.subjects.routerNoMatch.next({ message, content })
          }
          return
        }
        // Default args to empty string
        if (command.params) {
          for (let name of Object.keys(command.params)) {
            if (routingArgsObj[name] == null) {
              routingArgsObj[name] = ''
            }
          }
        }

        log.info({ key: command.key }, 'Message routed')

        const cancelDeferred = pDefer<void>()
        let messageDeleted = false
        const observables: ICommandInvocationObservables = {
          cancel: cancelDeferred.promise
        }
        const markAsCancelled = () => {
          messageDeleted = true
          cancelDeferred.resolve()
        }

        const send: ISend = (async (
          commandResult: CommandInvocationResult
        ): Promise<void | Message | Message[]> => {
          const { log } = this
          // command result is undefined
          if (commandResult == null) {
            log.info(
              { commandResult: JSON.stringify(commandResult) },
              'command returned nully'
            )
            return undefined
          }

          // command result is an instance of Message
          if (commandResult instanceof Message) {
            this.subjects.reply.next({ message, reply: commandResult })
            return undefined
          }

          // command result is a string (message must fit Discord message limit)
          if (typeof commandResult === 'string') {
            log.info({ commandResult }, 'command returned value')
            if (commandResult === '') {
              log.error('returned empty string result')
              return
            }
            const reply = await channel.send(commandResult)
            this.subjects.reply.next({ message, reply })
            return reply
          }

          // command result is an array of ISendMessage
          if (Array.isArray(commandResult)) {
            ok(commandResult.length !== 0)
            if (commandResult.length === 1) {
              // Single ISendMessage, fall through object case.
              commandResult = commandResult[0]
            } else {
              // Send paginated (ISendMessage | MessageOptions) objects.
              const reply = await this.sendPaginatedMessage(
                channel,
                commandResult
              )
              this.subjects.reply.next({ message, reply })
              return reply
            }
          }

          if (commandResult === REROUTE_HELP) {
            log.info('command rerouting to help topic')
            await this.processMessageContent({
              message,
              content: `help ${content}`,
              prefix
            })
            return undefined
          }

          // command must be an object
          assertIsObject(commandResult)

          if (isRerouteObject(commandResult)) {
            // Should reroute
            log.info(commandResult, 'command rerouting')
            await this.processMessageContent({
              message,
              content: commandResult.reroute,
              prefix
            })
            return undefined
          } else if (isSendMessage(commandResult)) {
            // command result is an ISendMessage
            const reply = await channel.send(
              commandResult.content,
              commandResult.options
            )
            this.subjects.reply.next({ message, reply })
            return reply
          } else {
            // command result is a MessageOptions
            const reply = await channel.send(commandResult)
            this.subjects.reply.next({ message, reply })
            return reply
          }
        }) as ISend

        const context: ICommandContext = {
          message,
          args: routingArgsObj,
          app: this,
          channel,
          prefix,
          observables,
          send,
          log: log.child({ command: command.title || command.key.toString() })
        }

        // Listen for events on invoking message.
        const messageDeleteSubscription = this.clientObservables.messageDelete
          .pipe(filter(m => message.id === m.id))
          .subscribe(markAsCancelled)

        // ### Invoke command. ###
        log.debug('Invoking command')
        let commandResult = await command.invoke(context)

        // Check if invoking command still exists (hasn't been deleted).
        messageDeleteSubscription.unsubscribe()
        if (messageDeleted) {
          log.info(
            'Invoking message has been deleted, ignoring command invocation output.'
          )
          return
        }

        await send(commandResult)
      } catch (err) {
        log.error(fullStack(err) || err)
        if (err.name === 'TimeoutError' || err.name === 'CancelError') {
          err = new PublicError(err.message, err)
        }
        this.subjects.invocationError.next({
          message,
          error: err
        })
      }
    } else {
      this.subjects.unrouted.next(message)
    }
  }

  addCommand(...commands: Command[]) {
    this._commands.push(...commands)
    this.traverseCommands('', commands)
  }

  /**
   * Adds one or more async functions that will be called
   * when the app is destroyed.
   */
  addAsyncTeardown(...teardownFns: (() => Promise<void>)[]) {
    this.asyncTeardownFns.push(...teardownFns)
  }

  private traverseCommands(
    prefix: ExtensiblePath<IRouteContext> | string,
    commands: ReadonlyArray<Command>
  ) {
    const { router } = this

    for (let command of commands) {
      if (isActionCommand(command)) {
        // Check if the command has any required parameters.
        if (
          command.params &&
          Object.values(command.params).some(
            p => p.type === 'channel' || p.type === 'param'
          )
        ) {
          // The command has some required parameter.
          // Create help for when the parameter is not provided.
          this.subjects.requestHelpCommand.next({
            command,
            path: concatExtensiblePath(prefix, command.key),
            prefix
          })
          // Route command.
          const path = concatExtensiblePath<IRouteContext>(
            prefix,
            command.key,
            ...buildParams(command.params)
          )
          router.set(path, command)
        } else {
          // The command doesn't have any required parameters.

          // Empty command with no parameters.
          const emptyPath = concatExtensiblePath(prefix, command.key)
          router.set(emptyPath, command)

          // Command with some parameters.
          if (command.params && Object.keys(command.params).length > 0) {
            const pathWithParams = concatExtensiblePath(
              prefix,
              command.key,
              ...buildParams(command.params)
            )
            router.set(pathWithParams, command)
          }
        }
      } else {
        // Show help for command if it doesn't have an invoke command
        this.subjects.requestHelpCommand.next({
          command,
          path: concatExtensiblePath(prefix, command.key),
          prefix
        })
      }
      if (command.children) {
        // The space is to separate the command key pieces
        const newPrefix = concatExtensiblePath(prefix, command.key, ' ')
        this.traverseCommands(newPrefix, command.children)
      }
      this.subjects.commandAdded.next({
        command,
        path: concatExtensiblePath(prefix, command.key),
        prefix
      })
    }
  }

  /**
   * Completes this app's observables,
   * and calls `destroy` on the client.
   */
  async destroy() {
    await Promise.all(this.asyncTeardownFns.map(td => this.call(td)))

    let subject: Subject<any>
    for (subject of Object.values(this.clientSubjects)) {
      subject.complete()
    }

    await this.client.destroy()
  }

  private async sendPaginatedMessage(
    channel: TextChannel | DMChannel | GroupDMChannel,
    pages: (ISendMessage | MessageOptions)[]
  ): Promise<Message> {
    const { client, log } = this

    const firstPage = normalizeSendMessage(pages[0])
    const pagesLastIndex = pages.length - 1

    const message = (await channel.send(
      firstPage.content,
      firstPage.options
    )) as Message

    await message.react(EMOJI_L)
    await message.react(EMOJI_R)

    let pageIndex = 0

    const sub = this.clientObservables.messageReactionAdd
      .pipe(filter(([, u]) => !(u.bot || u.id === client.user.id)))
      .subscribe(async ([mr, u]) => {
        const emojiName = mr.emoji.name

        let newPageIndex

        if (emojiName === EMOJI_L) {
          log.debug('reacted with prev')
          newPageIndex = Math.max(pageIndex - 1, 0)
        } else if (emojiName === EMOJI_R) {
          log.debug('reacted with next')
          newPageIndex = Math.min(pageIndex + 1, pagesLastIndex)
        } else {
          return
        }

        await mr.remove(u)

        if (newPageIndex === pageIndex) {
          return
        }
        pageIndex = newPageIndex
        const page = normalizeSendMessage(pages[pageIndex])
        await message.edit(page.content, page.options)
      })

    this.client.setTimeout(() => sub.unsubscribe(), 3.6e6)

    return message
  }

  private createObservables(): AppObservables {
    const ret: Partial<AppObservables> = {}
    for (let [key, value] of Object.entries(this.subjects)) {
      ret[key] = value.asObservable()
    }
    return ret as AppObservables
  }

  /**
   * Call a function,
   * and emit the error on the `error` observable
   * if an error occurs.
   */
  public async call(fn: () => MaybePromise<void>): Promise<void> {
    try {
      await fn()
    } catch (err) {
      this.subjects.error.next(err)
    }
  }
} // class App

const createClientObservables = weakMemoizeUnary(
  createClientObservablesUncached
)

function createClientObservablesUncached(client: Client): ClientObservables {
  const observables = {
    message: fromEvent<Message>(client, 'message'),
    messageReactionAdd: fromEvent<[MessageReaction, User]>(
      client,
      'messageReactionAdd',
      (mr: MessageReaction, u: User) => [mr, u]
    ),
    messageDelete: fromEvent<Message>(client, 'messageDelete'),
    ready: fromEvent<void>(client, 'ready'),
    error: fromEvent<Error>(client, 'error'),
    warn: fromEvent<string>(client, 'warn')
  }
  return observables
}

function appObserveClient(
  clientSubjects: IClientSubjects,
  observables: ClientObservables
): Subscription {
  const sub = new Subscription()
  for (let [key, observable] of Object.entries(observables)) {
    const subject = clientSubjects[key as keyof IClientSubjects]
    const s = (observable as any).subscribe(subject)
    sub.add(s)
  }
  return sub
}

function getObservablesFromSubjects(
  subjects: IClientSubjects
): ClientObservables {
  let ret: Partial<ClientObservables> = {}
  for (let [key, subject] of Object.entries(subjects)) {
    ret[key] = subject.asObservable()
  }
  return ret as ClientObservables
}
