import { DMChannel, GroupDMChannel, Message, MessageOptions, MessageReaction, StringResolvable, TextChannel, User } from 'discord.js';
import { Logger } from 'pino';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ExtensiblePath, ExtensiblePathComponent } from 'xerpath';
import { App } from './index';

/**
 * This symbol should be returned from a command invocation
 * to reroute to the help command.
 */
export const REROUTE_HELP: unique symbol = Symbol()

/**
 * The type of the object passed to a command invocation.
 */
export interface ICommandContext {
  /**
   * The message that triggered this command.
   */
  message: Message
  /**
   * Arguments matched by the router.
   */
  args: Record<string, string>
  /**
   * The app instance.
   */
  app: App
  /**
   * The channel of the message.
   */
  channel: TextChannel | DMChannel | GroupDMChannel
  /**
   * The command prefix stripped before routing the command.
   */
  prefix: string
  /**
   * Used to manually send a message and the app recognize the reply.
   */
  send: ISend
  /**
   * A child logger with additional information about the command
   * that can be used to log.
   */
  log: Logger
  /**
   * Observables related to the command invocation.
   */
  observables: ICommandInvocationObservables
}

/**
 * The type of the function that can be called from a command invocation
 * to send a message associated with that specific command invocation
 */
export interface ISend {
  (
    commandResult: void | Message | typeof REROUTE_HELP | IRerouteObject
  ): Promise<void>
  (commandResult: string | ISendMessage | MessageOptions): Promise<
    Message | Message[]
  >
  (commandResult: (MessageOptions | ISendMessage)[]): Promise<Message[]>
  (commandResult: CommandInvocationResult): Promise<void | Message | Message[]>
}

export interface IParameterBaseDefinition {
  description?: string
}

export interface IRestParameterDefinition extends IParameterBaseDefinition {
  type: 'rest'
}

export interface IDiscordChannelParameterDefinition
  extends IParameterBaseDefinition {
  type: 'channel'
}

export interface ISingleParameterDefinition extends IParameterBaseDefinition {
  type: 'param'
}

export interface IOptionalParameterDefinition extends IParameterBaseDefinition {
  type: 'optional'
}

export type ParameterDefinition =
  | IRestParameterDefinition
  | IDiscordChannelParameterDefinition
  | ISingleParameterDefinition
  | IOptionalParameterDefinition

export interface ICommand {
  title?: string
  description?: string
  key: ExtensiblePath<IRouteContext> | string
  children?: ICommand[]
  /**
   * Hide command from commands list.
   */
  hidden?: true
  usage?: string
}

/**
 * The type of the command with an invocation.
 */
export interface IActionCommand extends ICommand {
  // metadata
  invoke(context: ICommandContext): MaybePromise<CommandInvocationResult>
  params?: Record<string, ParameterDefinition>
}

/**
 * The type of the object that can be returned from a command invocation.
 */
export type CommandInvocationResult =
  | void
  | string
  | typeof REROUTE_HELP
  | IRerouteObject
  | MessageOptions
  | (MessageOptions | ISendMessage)[]
  | ISendMessage
  | Message

/**
 * The type of an object returned from a command invocation
 * to route to the help command for another command.
 */
export interface IRerouteObject {
  reroute: string
}

export interface ISendMessage {
  content?: StringResolvable
  options?: MessageOptions
}

export interface ITopLevelCommand extends ICommand {
  // metadata
  invoke?: never
}

export interface ICommandInvocationObservables {
  cancel: Promise<void>
}

export type MaybePromise<T> = T | PromiseLike<T>

export type Command = ITopLevelCommand | ICommand

export interface IRouteContext {
  rest(name: string): ExtensiblePathComponent
  param(name: string): ExtensiblePathComponent
  channel(name: string): ExtensiblePathComponent
  optional(name: string): ExtensiblePathComponent
}

export interface IClientSubjects {
  message: Subject<Message>
  messageReactionAdd: Subject<[MessageReaction, User]>
  messageDelete: Subject<Message>
  ready: Subject<void>
  error: Subject<Error>
  warn: Subject<string>
}

export type ClientObservables = {
  [Key in keyof IClientSubjects]: ObservableOnly<IClientSubjects[Key]>
}

export type AppObservables = {
  [Key in keyof IAppSubjects]: ObservableOnly<IAppSubjects[Key]>
}

export interface IAppSubjects {
  invocationError: Subject<{ message: Message; error: any }>
  reply: Subject<{ message: Message; reply: Message | Message[] }>
  unrouted: Subject<Message>
  requestHelpCommand: Subject<{
    command: Command
    path: ExtensiblePath<IRouteContext> | string
    prefix: ExtensiblePath<IRouteContext> | string
  }>
  commandAdded: Subject<{
    command: Command
    path: ExtensiblePath<IRouteContext> | string
    prefix: ExtensiblePath<IRouteContext> | string
  }>
  error: Subject<any>
  routerNoMatch: Subject<{ message: Message; content: string }>
  nameChange: BehaviorSubject<string>
}

type ObservableOnly<T> = T extends Observable<infer U> ? Observable<U> : never
