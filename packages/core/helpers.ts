import { ok } from 'assert'
import { MessageOptions } from 'discord.js'
import { ExtensiblePath } from 'xerpath'
import {
  Command,
  IActionCommand,
  IRerouteObject,
  IRouteContext,
  ISendMessage,
  ParameterDefinition
} from './types'

/**
 * Match all strings in regular expression.
 */
export function* matchAll(regex: RegExp, str: string) {
  let m: RegExpExecArray | null = null
  do {
    let m = regex.exec(str)
    if (m) {
      yield m[0]
    }
  } while (m)
}

export function isActionCommand(command: Command): command is IActionCommand {
  return typeof (command as IActionCommand).invoke === 'function'
}

export function isRerouteObject<T>(
  value: T | IRerouteObject
): value is IRerouteObject {
  return Boolean((value as IRerouteObject).reroute)
}

export function isSendMessage<T extends {}>(
  value: T | ISendMessage
): value is ISendMessage {
  return 'content' in value
}

export function assertIsObject<T extends {}>(value: T): void {
  ok(typeof value === 'object')
}

export function normalizeSendMessage(
  value: MessageOptions | ISendMessage
): ISendMessage {
  if (isSendMessage(value)) {
    return value
  }
  return { options: value }
}

export function createRouteContext(): IRouteContext {
  // describe generated path component
  const n = <F extends Function>(f: F, name: string): F => {
    f.toString = () => name
    return f
  }
  // regex path template
  const regex = (
    regexFn: () => RegExp,
    nameTransform: (name: string) => string = x => x
  ) => (name: string) =>
    n((s: string) => {
      // Remove space from start
      s = trimLeadingWhitespace(s)
      const regex = regexFn()
      const result = s.match(regex)
      if (result == null) {
        return null
      }
      const ret = {
        value: [name, result[0]],
        remainingPath: s.substr(result[0].length)
      }
      return ret
    }, nameTransform(name))

  const rest = (name: string) =>
    n((s: string) => ({ value: [name, s], remainingPath: '' }), ` {${name}*}`)

  const param = regex(() => /[^\s]+/, name => ` {${name}}`)

  const optional = regex(() => /[^\s]*/, name => ` {${name}?}`)

  const channel = regex(() => /(?:\d+|<#\d+>)/, name => ` <#${name}>`)

  return {
    rest,
    param,
    channel,
    optional
  }
}

function trimLeadingWhitespace(s: string): string {
  return s.replace(/^\s+/, '')
}

export function buildParams(
  params: Record<string, ParameterDefinition> = {}
): (ExtensiblePath<IRouteContext> | string)[] {
  const ret: (ExtensiblePath<IRouteContext> | string)[] = []
  for (let [name, def] of Object.entries(params)) {
    switch (def.type) {
      case 'rest': {
        ret.push(r => r`${r.rest(name)}`)
        break
      }
      case 'channel': {
        ret.push(r => r`${r.channel(name)}`)
        break
      }
      case 'param': {
        ret.push(r => r`${r.param(name)}`)
        break
      }
      case 'optional': {
        ret.push(r => r`${r.optional(name)}`)
        break
      }
    }
  }
  return ret
}
