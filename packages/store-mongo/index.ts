import {
  MessageMetadataStore,
  Store,
  UserStore,
  ModuleStore
} from '@hayato/store'
import { Message } from 'discord.js'
import { Db } from 'mongodb'
import { PassThrough } from 'stream'

const DEFAULT_PREFIX = 'hayato_'

export class MongoStore implements Store {
  readonly db: Db
  readonly prefix: string
  constructor({ db, prefix = DEFAULT_PREFIX }: { db: Db; prefix?: string }) {
    this.db = db
    this.prefix = prefix
  }
  moduleStore = new MongoModuleStore(this)
  userStore = new MongoUserStore(this)
  messageMetadataStore = new MongoMessageMetadataStore(this)
}

export class MongoModuleStore implements ModuleStore {
  collectionName = 'modules'
  get collection() {
    return this.parent.db.collection(
      `${this.parent.prefix}${this.collectionName}`
    )
  }
  constructor(public readonly parent: MongoStore) {}

  async getConfig(moduleKey: string): Promise<unknown> {
    const doc = await this.collection.findOne({ _id: moduleKey })
    if (!doc) {
      return undefined
    }
    return doc.value
  }
}

export class MongoUserStore implements UserStore {
  collectionName = 'users'
  get collection() {
    return this.parent.db.collection(
      `${this.parent.prefix}${this.collectionName}`
    )
  }
  constructor(public readonly parent: MongoStore) {}

  async getValue(
    userId: string,
    moduleKey: string,
    key: string
  ): Promise<unknown> {
    const doc = await this.collection.findOne({
      _id: { userId, moduleKey, key }
    })
    if (!doc) return undefined
    return doc.value
  }

  async setValue(
    userId: string,
    moduleKey: string,
    key: string,
    value: any
  ): Promise<void> {
    await this.collection.updateOne(
      { _id: { userId, moduleKey, key } },
      { $set: { value } },
      { upsert: true }
    )
  }
}

const messageMetadataSeenDbs = new WeakSet<Db>()

export class MongoMessageMetadataStore implements MessageMetadataStore {
  collectionName = 'messages'
  get collection() {
    return this.parent.db.collection(
      `${this.parent.prefix}${this.collectionName}`
    )
  }

  constructor(public readonly parent: MongoStore) {}

  async setValue(
    message: Message,
    moduleKey: string,
    key: string,
    value: any
  ): Promise<void> {
    if (!messageMetadataSeenDbs.has(this.parent.db)) {
      await this.collection.createIndex(
        { messageId: 1, moduleKey: 1, key: 1 },
        { unique: true }
      )
      messageMetadataSeenDbs.add(this.parent.db)
    }
    await this.collection.updateOne(
      { messageId: message.id, moduleKey, key },
      { $set: { value } },
      { upsert: true }
    )
  }

  async getValue(
    message: Message,
    moduleKey: string,
    key: string
  ): Promise<void> {
    const doc = await this.collection.findOne({
      messageId: message.id,
      moduleKey,
      key
    })
    if (!doc) return undefined
    return doc.value
  }

  private async *getDocs(
    message: Message,
    moduleKey: string
  ): AsyncIterableIterator<MessageMetadataDocument> {
    const cursor = this.collection.find({ messageId: message.id, moduleKey })
    const pt = new PassThrough({ objectMode: true })
    cursor.pipe(pt)
    return pt[Symbol.asyncIterator]()
  }

  async *iterateValues(
    message: Message,
    moduleKey: string
  ): AsyncIterableIterator<[string, unknown]> {
    for await (let doc of this.getDocs(message, moduleKey)) {
      yield [doc._id.moduleKey, doc.value]
    }
  }
}

export interface MessageMetadataDocument<TValue = unknown> {
  _id: {
    messageId: string
    moduleKey: string
    key: string
  }
  value: TValue
}
