# config-mongo

Use MongoDB as a store

## Usage

```ts
import { MongoStore } from '@hayato/store-mongo'

const store = new MongoStore()
```

Patch an existing store object:

```ts
import { MongoStore } from '@hayato/store-mongo'

const mongoStore = new MongoStore()

store.moduleStore = mongoStore.moduleStore
store.userStore = mongoStore.userStore
store.messageMetadataStore = mongoStore.messageMetadataStore
```

## Schema

The prefix `hayato_` can be changed on the store.

* hayato_modules - module config
  * _id = moduleKey
  * value
* hayato_users - user store
  * _id: { userId, moduleKey, key }
  * value
* hayato_messages - message metadata store. A unique index is created for `{ messageId, moduleKey, key }`.
  * _id: default
  * messageId
  * moduleKey
  * key
  * value
