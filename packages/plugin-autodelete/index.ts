import { App } from '@hayato/core'
import { Store } from '@hayato/store'
import { DMChannel, GroupDMChannel, Message, TextChannel } from 'discord.js'
import pMap from 'p-map'

const PLUGIN_KEY = 'autodelete'

export function autodelete(app: App, { store }: { store: Store }) {
  const log = app.log.child({ module: 'plugin-autodelete' })

  app.observables.reply.subscribe(({ message, reply }) =>
    app.call(async () => {
      await pMap(([] as Message[]).concat(reply), async (reply: Message) => {
        const meta: Meta = {
          channelId: reply.channel.id
        }
        await store.messageMetadataStore.setValue(
          message,
          PLUGIN_KEY,
          reply.id,
          meta
        )
      })
    })
  )

  app.clientObservables.messageDelete.subscribe(message =>
    app.call(async () => {
      const promises: Promise<void>[] = []
      for await (let [
        replyId,
        meta
      ] of store.messageMetadataStore.iterateValues(
        message,
        PLUGIN_KEY
      ) as AsyncIterableIterator<[string, Meta]>) {
        promises.push(
          (async () => {
            const channel = app.client.channels.get(meta.channelId)
            if (!channel) {
              return
            }
            if (
              !(
                channel instanceof TextChannel ||
                channel instanceof DMChannel ||
                channel instanceof GroupDMChannel
              )
            ) {
              return
            }

            const reply = await channel.fetchMessage(replyId)
            if (!reply) {
              return
            }
            if (reply.deletable) {
              log.debug({ replyId, messageId: message.id }, `Deleting reply`)
              await reply.delete()
            }
          })()
        )
      }
      await Promise.all(promises)
    })
  )
}

export default autodelete

interface Meta {
  channelId: string
}
