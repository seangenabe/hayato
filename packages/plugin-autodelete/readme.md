# plugin-autodelete

Automatically delete replies

## Usage

```ts
import autodelete from '@hayato/plugin-autodelete'

autodelete(app, { store })
```
