# healthz

Health check

## Usage

```ts
import healthz from '@hayato/healthz'

healthz(app, { port: 8080 })
```

### API

#### healthz
```ts
export function healthz(app: App, { port = 8080 }: { port?: number }): void
export default healthz
```

Adds a health-checking HTTP server to the app.

The health check will succeed if `Client#readyAt` is available. (A failing check should indicate an initial faulty connection with Discord, e.g. bad token.)
