import { createServer, Server } from 'http'
import { App } from '@hayato/core'
import { promisify } from 'util'

const healthzServers = new WeakMap<App, Server>()

function getHealthzForApp(app: App) {
  const server1 = healthzServers.get(app)
  if (server1) {
    return server1
  }
  const server2 = createServer((_, res) => {
    res.setHeader('content-type', 'text/plain')
    if (app.client.readyAt) {
      res.end('ok')
    } else {
      res.statusCode = 500
      res.end('err')
    }
  })
  return server2
}

export function healthz(app: App, { port = 8080 }: { port?: number } = {}) {
  const server = getHealthzForApp(app)

  const listen = promisify<number>(
    (port: number, cb: (err: Error | null | undefined) => void) =>
      server.listen(port, cb)
  )
  const close = promisify<void>(cb => server.close(cb))

  app.addAsyncTeardown(close)

  app.call(() => listen(port))
}

export default healthz
