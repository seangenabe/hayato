import { App } from '@hayato/core'
import { normalizeSendMessage } from '@hayato/core/helpers'
import { ISendMessage } from '@hayato/core/types'
import {
  DMChannel,
  GroupDMChannel,
  Message,
  MessageOptions,
  TextChannel
} from 'discord.js'
import { nn } from 'non-null'
import { map as mapIterable } from 'starry.map'

/**
 * Sends a message,
 * and listens for reactions associated with the provided actions for a match
 * to run them.
 */
export async function attachActionsToMessage<TAction extends string>(
  opts: AttachOptions<TAction>
) {
  const { actions, app, message } = opts

  // Listen for reactions and do actions when reacted upon.
  app.clientObservables.messageReactionAdd.subscribe(([mr, u]) => {
    if (
      app.client !== message.client ||
      u.bot ||
      u.id === app.client.user.id ||
      mr.message.id !== message.id
    ) {
      return
    }
    let action: (() => MaybePromise<void>) | undefined
    if (mr.emoji.id == null) {
      action = actions.get(mr.emoji.name as TAction)
    } else {
      action = actions.get(mr.emoji.id as TAction)
    }
    if (!action) {
      return
    }
    app.call(async () => {
      await Promise.all([mr.remove(u), action!()])
    })
  })

  // Add reactions to message.
  for (let key of actions.keys()) {
    if (key.length <= 2) {
      await message.react(key)
      continue
    }
    const emoji = app.client.emojis.get(key)
    if (emoji) {
      await message.react(emoji)
    } else {
      await message.react(key)
    }
  }
}

/**
 * Sends a message,
 * then switches between provided pages depending on the current state
 * determined by reacting to the message.
 */
export async function attachPagesToMessage<TAction extends string>({
  app,
  message,
  pages
}: PagesOptions<TAction>) {
  attachActionsToMessage({
    app,
    message,
    actions: new Map(
      mapIterable(pages, ([action, page]) => {
        const p = normalizeSendMessage(page)
        const value = async () => {
          await message.edit(p.content, p.options)
        }
        return [action, value] as [string, typeof value]
      })
    )
  })
}

export async function sendMessageWithPages<TAction extends string>({
  app,
  channel,
  pages,
  initialAction
}: SendPagesOptions<TAction>) {
  const initialPage = normalizeSendMessage(nn(pages.get(initialAction)))
  const message = await channel.send(initialPage.content, initialPage.options)
  if (Array.isArray(message)) {
    throw new Error('send result cannot be an array.')
  }
  await attachPagesToMessage<TAction>({ app, message, pages })
}

const EMOJI_L = '◀'
const EMOJI_R = '▶'

/**
 * Sends a message,
 * then scrolls through different pages using left and right arrow reactions.
 */
export async function attachPaginationToMessage({
  app,
  message,
  pages
}: PaginationOptions) {
  let pageIndex = 0
  const pages2 = pages.map(p => normalizeSendMessage(p))
  const pages2LastIndex = pages2.length - 1
  await attachActionsToMessage({
    app,
    message,
    actions: new Map<typeof EMOJI_L | typeof EMOJI_R, () => Promise<void>>([
      [
        EMOJI_L,
        async () => {
          if (pageIndex <= 0) {
            return
          }
          pageIndex--
          const currentPage = pages2[pageIndex]
          await message.edit(currentPage.content, currentPage.options)
        }
      ] as [typeof EMOJI_L, () => Promise<void>],
      [
        EMOJI_R,
        async () => {
          if (pageIndex >= pages2LastIndex) {
            return
          }
          pageIndex++
          const currentPage = pages2[pageIndex]
          await message.edit(currentPage.content, currentPage.options)
        }
      ] as [typeof EMOJI_R, () => Promise<void>]
    ])
  })
}

export async function sendPaginatedMessage({
  app,
  channel,
  pages
}: SendPaginatedOptions) {
  const initialPage = normalizeSendMessage(nn(pages[0]))
  const message = await channel.send(initialPage.content, initialPage.options)
  if (Array.isArray(message)) {
    throw new Error('send result cannot be an array')
  }
  await attachPaginationToMessage({ app, message, pages })
}

interface AttachOptions<TAction extends string = string, TResult = void> {
  app: App
  message: Message
  actions: Map<TAction, () => MaybePromise<TResult>>
}

export type MessagePage = ISendMessage | MessageOptions

export interface PagesOptions<TAction extends string = string> {
  app: App
  message: Message
  pages: Map<TAction, MessagePage>
}

export interface SendPagesOptions<TAction extends string = string> {
  app: App
  channel: TextChannelLike
  pages: Map<TAction, MessagePage>
  initialAction: TAction
}

export interface PaginationOptions {
  app: App
  message: Message
  pages: MessagePage[]
}

export interface SendPaginatedOptions {
  app: App
  channel: TextChannelLike
  pages: MessagePage[]
}

type MaybePromise<T> = Promise<T> | T

type TextChannelLike = TextChannel | DMChannel | GroupDMChannel
