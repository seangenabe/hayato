import { safeLoad } from 'js-yaml'
import { Store, ModuleStore } from '@hayato/store'
import { promisify } from 'util'
import { readFile } from 'fs'
const readFileAsync = promisify(readFile)

export class YamlStore implements Store {
  public readonly path: string
  private config: { [key: string]: unknown }

  constructor({ path = 'hayato.yml' }: { path?: string }) {
    this.path = path
  }

  async getConfig(): Promise<{ [key: string]: unknown }> {
    if (this.config) {
      return this.config
    }
    const fileContents = await readFileAsync(this.path, { encoding: 'utf8' })
    this.config = safeLoad(fileContents, { filename: this.path })
    return this.config
  }

  moduleStore = new YamlModuleStore(this)
  get userStore(): never {
    throw new Error()
  }
  set userStore(v: never) {
    throw new Error()
  }
  get messageMetadataStore(): never {
    throw new Error()
  }
  set messageMetadataStore(v: never) {
    throw new Error()
  }
}

export class YamlModuleStore implements ModuleStore {
  constructor(public readonly parent: YamlStore) {}
  async getConfig(moduleKey: string): Promise<unknown> {
    const config = await this.parent.getConfig()
    return config[moduleKey]
  }
}
