# store-yaml

YAML module config

## Usage

```ts
import { YamlStore } from '@hayato/store-yaml'

const yamlStore = new YamlStore(`${__dirname}/hayato.yml`)
store.moduleStore = yamlStore.moduleStore
```
