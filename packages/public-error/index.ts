import { BaseError } from 'make-error-cause'

export class PublicError extends BaseError {
  constructor(message: string, cause?: Error) {
    super(message, cause)
  }
}
