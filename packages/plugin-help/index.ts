import { App } from '@hayato/core'
import { Command, IRouteContext, IActionCommand } from '@hayato/core/types'
import {
  ExtensiblePath,
  concatExtensiblePath,
  ExtensiblePathComponent
} from 'xerpath'
import { RichEmbedOptions, MessageOptions } from 'discord.js'
import { isActionCommand, buildParams } from '@hayato/core/helpers'
import { ITheme } from '@hayato/theme'

export function help(
  app: App,
  {
    theme,
    helpCommandPrefix = 'help '
  }: { theme: ITheme; helpCommandPrefix?: string }
) {
  // Add help command on request help (non-hidden, parameter required)
  app.observables.requestHelpCommand.subscribe(({ command, path, prefix }) => {
    const helpCommand = createHelpForCommand({
      app,
      command,
      prefix,
      color: theme.neutralCommand,
      router: app.router
    })
    app.router.set(path, helpCommand)
  })
  // Add help command for command (help + command key)
  app.observables.commandAdded.subscribe(({ command, prefix, path }) => {
    const helpPath = concatExtensiblePath(helpCommandPrefix, path)
    const helpCommand = createHelpForCommand({
      app,
      command,
      prefix,
      color: theme.neutralCommand,
      router: app.router
    })
    app.router.set(helpPath, helpCommand)
  })
  // Send message on no match (top-level not matched)
  app.observables.routerNoMatch.subscribe(({ message }) => {
    const prefix = app.getPrefix(message)
    const { channel } = message
    channel.send(
      `Command not recognized. Type \`${prefix}help\` to see the list of available commands.`
    )
  })

  // Add normal help command ("help")
  const helpCommand: IActionCommand = {
    title: 'Help',
    key: 'help',
    description: 'Get help information.',
    invoke: async function helpFn({ app, prefix }) {
      const embed: RichEmbedOptions = {
        title: `${app.name} Help`,
        color: theme.neutralCommand,
        description: `Type \`${prefix}help <command>\` to get help on a command.`,
        fields: [
          {
            name: 'Available commands',
            value: app.commands
              .filter(c => !c.hidden)
              .map(c => `\`${c.key.toString()}\``)
              .join(' - ')
          }
        ]
      }
      return { embed }
    }
  }
  app.addCommand(helpCommand)
}

export default function createHelpForCommand({
  app,
  command,
  prefix,
  color,
  router
}: {
  app: App
  command: Command
  prefix: ExtensiblePath<IRouteContext> | string
  color?: string | number
  router: App['router']
}): IActionCommand {
  // Call help on command
  const helpCommand: IActionCommand = {
    title: `help for command [${command.title || `${prefix}${command.key}`}]`,
    key: '',
    description: '(auto-generated)',
    invoke: () => {
      // Unused for meta-command
      const builtKeyString = getBuildKeyString(
        app,
        concatExtensiblePath(prefix, command.key)
      )
      const fields: { name: string; value: string; inline?: boolean }[] = []
      const embed: RichEmbedOptions = {
        title: command.title || builtKeyString,
        description: command.description || builtKeyString,
        fields,
        color
      }
      if (
        isActionCommand(command) &&
        command.params != null &&
        Object.keys(command.params).length > 0
      ) {
        const paramsStr = Object.entries(command.params)
          .map(([name, def]) => {
            return `\`${name}\` ${
              def.description ? ` - ${def.description}` : ''
            }`
          })
          .join('\n')
        const path = concatExtensiblePath<IRouteContext>(
          prefix,
          command.key,
          ' ',
          ...buildParams(command.params)
        )
        const builtPath = router.buildPath(path)
        const usageLine = builtPath.join('')
        fields.push({
          name: 'Syntax',
          value: `\`${usageLine}\`\n${paramsStr}`
        })
      }
      const visibleChildren =
        (command.children && command.children.filter(c => !c.hidden)) || []
      if (visibleChildren.length !== 0) {
        fields.push({
          name: 'Available sub-commands',
          value: visibleChildren
            .map(child => {
              const combinedKey = concatExtensiblePath(
                command.key,
                ' ',
                child.key
              )
              return `\`${getBuildKeyString(app, combinedKey)}\``
            })
            .join(' - ')
        })
      }
      const opts: MessageOptions = { embed }
      return opts
    }
  }
  return helpCommand
}

function getBuildKeyString(
  app: App,
  key: ExtensiblePath<IRouteContext> | string
) {
  return getKeyComponentsString(app.router.buildPath(key))
}

function getKeyComponentsString(
  pathComponents: (string | ExtensiblePathComponent)[]
) {
  return pathComponents.map(component => component.toString()).join('')
}
