# help

Help command provider

## Usage

```ts
import help from '@hayato/help'

help(app, { theme })
```
