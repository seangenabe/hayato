# theme

Theme support.

## Usage

To get the theme for an app, do:

```ts
import { App } from '@hayato/core'
import { getThemeForApp } from '@hayato/theme'

const app = new App()
const theme = getThemeForApp(app)

// Set values for the theme
theme.neutralCommand = '#5555ff'
```

### API

#### getThemeForApp

```ts
export function getThemeForApp(app: {}): ITheme
```

Get the theme for an App (or any object).

#### ITheme

```ts
export interface ITheme {
  neutralCommand: string
  successCommand: string
  failureCommand: string
}
```
