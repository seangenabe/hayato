import { weakMemoizeUnary } from 'weak-memoize-unary'

export const getThemeForApp = weakMemoizeUnary(() => createDefaultTheme())

export default getThemeForApp

function createDefaultTheme() {
  return {
    neutralCommand: 0x808080,
    successCommand: 0x33ff33,
    failureCommand: 0xff3333
  }
}

export interface ITheme {
  neutralCommand: string | number | undefined
  successCommand: string | number | undefined
  failureCommand: string | number | undefined
}
