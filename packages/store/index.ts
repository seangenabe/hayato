import { App } from '@hayato/core'
import { Message } from 'discord.js'
import { weakMemoizeUnary } from 'weak-memoize-unary'
import { InMemoryMessageMetadataStore } from './message-metadata-store'
import { InMemoryUserStore } from './user-store'

export const getStoreForApp = weakMemoizeUnary(createDefaultStoreForApp)

export default getStoreForApp

function createDefaultStoreForApp(app: App): Store {
  const log = app.log.child({ module: 'store' })
  const ret: Store = {
    moduleStore: {
      getConfig() {
        throw new Error('Not initialized')
      }
    },
    userStore: new InMemoryUserStore(log),
    messageMetadataStore: new InMemoryMessageMetadataStore(log)
  }
  return ret
}

export interface Store {
  // Module
  moduleStore: ModuleStore
  // User
  userStore: UserStore
  // Message metadata
  messageMetadataStore: MessageMetadataStore
}

export interface ModuleStore {
  getConfig(moduleKey: string): MaybePromise<unknown>
}

export interface UserStore {
  getValue(
    userId: string,
    moduleKey: string,
    key: string
  ): MaybePromise<unknown>
  setValue(
    userId: string,
    moduleKey: string,
    key: string,
    value: any
  ): MaybePromise<void>
}

export interface MessageMetadataStore {
  setValue(
    message: Message,
    moduleKey: string,
    key: string,
    value: any
  ): MaybePromise<void>
  iterateValues(
    message: Message,
    moduleKey: string
  ): AsyncIterableIterator<[string, unknown]>
  getValue(
    message: Message,
    moduleKey: string,
    key: string
  ): MaybePromise<unknown>
}

type MaybePromise<T> = T | PromiseLike<T>
