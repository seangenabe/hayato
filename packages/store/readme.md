# config

Abstract store

## Usage

This module exposes these config scopes that can be configured separately:

* module - per-module storage
* user - per-module per-user storage

This module allows for the following read and write modes:

* module - read
* user - read/write

To get the config store for your app, do:

```ts
import { App } from '@hayato/core'
import { getStoreForApp } from '@hayato/store

const app = new App()
const store = getStoreForApp(app)
```

### API

#### getStoreForApp 

```ts
export function getStoreForApp(app: App): Store
```

Get the store for an App. Creates a new store with some default values if it doesn't exist. Note that the default values for module and user config will throw an error.

#### Store

```ts
interface Store {
  getModuleConfig(moduleKey: string): MaybePromise<unknown>
  getUserConfigValue(
    userId: string,
    moduleKey: string,
    key: string
  ): MaybePromise<unknown>
  setUserConfigValue(
    userId: string,
    moduleKey: string,
    key: string,
    value: unknown
  ): MaybePromise<void>
}
```
