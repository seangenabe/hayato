import { Logger } from 'pino'
import { UserStore } from '.'

export class InMemoryUserStore implements UserStore {
  private store = new Map<string, Map<string, Map<string, unknown>>>()
  constructor(private log: Logger) {}

  private getL2Map(userId: string, moduleKey: string) {
    return getOrCreateMap(
      getOrCreateMap(this.store, userId, () => new Map()),
      moduleKey,
      () => new Map()
    )
  }

  getValue(userId: string, moduleKey: string, key: string) {
    return this.getL2Map(userId, moduleKey).get(key)
  }

  setValue(userId: string, moduleKey: string, key: string, value: any) {
    this.log.warn(
      'You are using an in-memory store. This may lead to memory leaks.'
    )
    this.getL2Map(userId, moduleKey).set(key, value)
  }

  async *iterateValues(userId: string, moduleKey: string) {
    return yield* this.getL2Map(userId, moduleKey)
  }
}

function getOrCreateMap<K, V>(map: Map<K, V>, key: K, createValue: () => V) {
  if (map.has(key)) {
    return map.get(key)!
  }
  const value = createValue()
  map.set(key, value)
  return value
}
