import { Message } from 'discord.js'
import { MessageMetadataStore } from '.'
import { Logger } from 'pino'

export class InMemoryMessageMetadataStore implements MessageMetadataStore {
  private messageMetadata = new Map<string, Map<string, any>>()
  constructor(private log: Logger) {}
  private getOuterStore(key1: string, key2: string): Map<string, any> {
    const key = JSON.stringify([key1, key2])
    if (this.messageMetadata.has(key)) {
      return this.messageMetadata.get(key)!
    }
    const map = new Map<string, any>()
    this.messageMetadata.set(key, map)
    return map
  }
  setValue(message: Message, moduleKey: string, key: string, value: any): void {
    this.log.warn(
      'You are using an in-memory store. This may lead to memory leaks.'
    )
    this.getOuterStore(message.id, moduleKey).set(key, value)
  }

  async *iterateValues(
    message: Message,
    moduleKey: string
  ): AsyncIterableIterator<[string, unknown]> {
    yield* this.getOuterStore(message.id, moduleKey).entries()
  }

  getValue(message: Message, moduleKey: string, key: string): unknown {
    return this.getOuterStore(message.id, moduleKey).get(key)
  }
}
